﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfApplication1
{
    /// <summary>
    /// Interaction logic for Event.xaml
    /// </summary>
    public partial class Event : Window
    {
        string connectionString = "server=aws-public-db-1.csq3gmb5turd.us-east-1.rds.amazonaws.com;user id=badge_printing;password=%P36khRf5D5QrW@t;Allow User Variables=True;persistsecurityinfo=False;database=badgeprint;SslMode=Required; ";
        string query = "SELECT e_name FROM e_vent";

        public Event()
        {

            InitializeComponent();
            LoadEvent();
            
        }
        private void LoadEvent()
        {

            try
            {

                MySqlConnection connection = new MySqlConnection(connectionString);
                connection.Open(); 
                MySqlCommand command = new MySqlCommand(query, connection);
                using (MySqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {

                            comboBox.Items.Add(reader.GetString(0));

                        }
                    }
                
                
            }
            catch (Exception)
            {

                MessageBox.Show("Unable to load data from database");

            }
        }

        private void comboBox_SelectedIndexChanged(object sender, SelectionChangedEventArgs e)
        {
            
        }

        private void textBox_MouseClick(object sender, TextChangedEventArgs e)
        {
           
        }

        private void textBox1_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
           
            try
            {

                if (comboBox.SelectedIndex != -1)
                {

                    Attendee w = new Attendee(comboBox.Text);
                    w.Show();
                    Close();

                }
                else if (textBox.Text == "Event Name...." || textBox1.Text == "Event code...." || textBox1.Text == "" || textBox.Text == "")
                {

                    MessageBox.Show("Please enter an Event Name AND an Event Number or choose an event");

                }
                else if (textBox.Text != "Event Name...." || textBox1.Text != "Event code...." || textBox1.Text != "" || textBox.Text != "")
                {

                    MySqlConnection connection = new MySqlConnection(connectionString);
                    MySqlCommand command = new MySqlCommand("INSERT INTO e_vent (e_id, e_name) VALUES(@e_id, @e_name)", connection);
                    command.Parameters.AddWithValue("@e_id", textBox1.Text);
                    command.Parameters.AddWithValue("@e_name", textBox.Text);
                    connection.Open();
                    command.ExecuteNonQuery();
                    MessageBox.Show("Congratulations! You have created a new Event!");
                    connection.Close();
                    Attendee w = new Attendee(textBox.Text);
                    w.Show();
                    Close();

                }
             
            }
            catch (Exception)
            {

                MessageBox.Show("This Event Code has already been created please enter a new Event Code or choose an Existing Event");

            }
  
        }

        private void TextBox_GotFocus(object sender, RoutedEventArgs e)
        {

            TextBox tb = (TextBox)sender;
            tb.Text = string.Empty;
            tb.GotFocus -= TextBox_GotFocus;

        }
    }
}
