﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualBasic.FileIO;
using MySql.Data.MySqlClient;
using System.Windows;
using System.Windows.Controls;
using iTextSharp.text;
using iTextSharp.text.pdf;
using OnBarcode.Barcode;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;
using System.Text.RegularExpressions;
using System.Data;
using iTextSharp.text.pdf.qrcode;
using iTextSharp.text.pdf.parser;
using System.Diagnostics;
using System.Drawing.Printing;
using Microsoft.Win32;
using PdfSharp.Pdf.Printing;
using System.Windows.Navigation;

namespace WpfApplication1
{
    /// <summary>
    /// Interaction logic for AttendeeList.xaml
    /// </summary>
    public partial class AttendeeList : Window
    {

        string connectionString = "server=aws-public-db-1.csq3gmb5turd.us-east-1.rds.amazonaws.com;user id=badge_printing;password=%P36khRf5D5QrW@t;Allow User Variables=True;persistsecurityinfo=False;database=badgeprint;SslMode=Required; ";
        string value;

        public AttendeeList(string _value)
        {

            InitializeComponent();
            value = _value;

        }

        private void listView1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {


        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            
            MySqlConnection con = new MySqlConnection(connectionString);
            MySqlDataAdapter ad = new MySqlDataAdapter();
            MySqlCommand cmd = new MySqlCommand("Select attend_id, a_fname, a_lname, a_email, as_district, reg_id, attendee.e_id , e_name From attendee left join e_vent on attendee.e_id = e_vent.e_id where e_name = '" + value + "' order by a_lname");
            ad.SelectCommand = cmd;
            cmd.Connection = con;
            DataTable dt = new DataTable();
            ad.Fill(dt);
            listView1.ItemsSource = dt.DefaultView;
            con.Close();

        }

        private void print_Click(object sender, RoutedEventArgs e)
        {
            BaseFont bft = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, false);
            BaseFont BOLD = BaseFont.CreateFont(BaseFont.HELVETICA_BOLD, BaseFont.CP1252, false);
            Font times = new Font(BOLD, 19);
            Font times1 = new Font(bft, 17);
            Document doc = new Document(new iTextSharp.text.Rectangle(250f, 792f));
            PdfWriter w = PdfWriter.GetInstance(doc, new FileStream("Badge.pdf", FileMode.Create));
            doc.Open();

            foreach (object element in listView1.SelectedItems)
            {

                DataRowView row = (DataRowView)element;
                iTextSharp.text.Paragraph p = new iTextSharp.text.Paragraph("\n\n" + row[1].ToString() + " " + row[2].ToString(), times);
                iTextSharp.text.Paragraph p1 = new iTextSharp.text.Paragraph(row[4].ToString() + "\n", times1);
                p.Alignment = Element.ALIGN_CENTER;
                p1.Alignment = Element.ALIGN_CENTER;
                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                OnBarcode.Barcode.QRCode qrcode = new OnBarcode.Barcode.QRCode();
                qrcode.Data = ("{\"event_code\":\"" + row[6].ToString() + "\",\"registration_id\":\"" + row[5].ToString() + "\",\"attendee_id\":\"" + row[0].ToString() + "\"}");
                qrcode.DataMode = QRCodeDataMode.Auto;
                // QR-Code format mode

                // Unit of meature for all size related setting in the library. 
                qrcode.UOM = UnitOfMeasure.PIXEL;
                // Bar module size (X), default is 2 pixel;
                qrcode.X = 2;

                // Image resolution in dpi, default is 70 dpi.
                qrcode.Resolution = 70;
                // Created barcode orientation.

                // Generate QR-Code and encode barcode to png format
                qrcode.ImageFormat = System.Drawing.Imaging.ImageFormat.Png;
                qrcode.drawBarcode("qrcode.png");

                iTextSharp.text.Image img = iTextSharp.text.Image.GetInstance("qrcode.png" + "\n\n");
                img.Alignment = Element.ALIGN_CENTER;
                /////////////////////////////////////////////////////////////////////changes////////////////////////////////////////////////////////////////////////
                OnBarcode.Barcode.QRCode qrcode2 = new OnBarcode.Barcode.QRCode();
                qrcode2.Data = (row[0].ToString());/////attend id just number no qoutes or strings. I know it says to.string 
                qrcode.DataMode = QRCodeDataMode.Auto;

                qrcode2.UOM = UnitOfMeasure.PIXEL;

                qrcode2.X = 2;
                qrcode2.Resolution = 70;

                qrcode2.ImageFormat = System.Drawing.Imaging.ImageFormat.Png;
                qrcode2.drawBarcode("qrsmall.png");
                iTextSharp.text.Image img2 = iTextSharp.text.Image.GetInstance("qrsmall.png");
                img2.SetAbsolutePosition(doc.PageSize.Width - 25f - 50f, doc.PageSize.Height - 150f - 216.6f);

                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                doc.Add(p);
                doc.Add(p1);
                doc.Add(img);
                doc.Add(img2);
                doc.NewPage();
            }
                doc.Close();

                PdfFilePrinter.AdobeReaderPath = @"C:\Program Files (x86)\Adobe\Acrobat Reader DC\Reader\AcroRd32.exe";

            // Present a Printer settings dialog to the user so that they may select the printer to use.
               PrinterSettings settings = new PrinterSettings();
                settings.Collate = false;
                PrintDialog printerDialog = new PrintDialog();
                var result = printerDialog.ShowDialog();

                if (result == true)
                {
                    // Print the document on the selected printer 
                    PdfFilePrinter printer = new PdfFilePrinter("Badge.pdf", settings.PrinterName);

                    try
                    {

                        printer.Print();

                    }
                    catch (Exception ex)
                    {

                        MessageBox.Show("Error: " + ex.Message);

                    }


                }

        }
        
        private void Select_all_Click(object sender, RoutedEventArgs e)
        {
            
                listView1.SelectAll();
        }

        private void Back_Click(object sender, RoutedEventArgs e)
        {
            
            Attendee a = new Attendee(value);
            a.Show();
            Close();
        }
    }
        
}
