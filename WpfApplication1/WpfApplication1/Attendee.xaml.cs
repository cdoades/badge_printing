﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualBasic.FileIO;
using MySql.Data.MySqlClient;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;
using System.Text.RegularExpressions;
using System.Data;

namespace WpfApplication1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class Attendee : Window
    {
        string connectionString = "server=aws-public-db-1.csq3gmb5turd.us-east-1.rds.amazonaws.com;user id=badge_printing;password=%P36khRf5D5QrW@t;Allow User Variables=True;persistsecurityinfo=False;database=badgeprint;SslMode=Required; ";


        public Attendee(string value)
        {
            InitializeComponent();
            EName.Text = value;
            LoadEvent();

        }

        private void LoadEvent()
        {
            ///Generates random attendee id
            Random random = new Random();
            aID.Text = (random.Next(1000, 9999)).ToString();
            ///Generates random registration id....note Accidently typed red instead of reg. might change it one day might not. you do the math.
            string randoms = Guid.NewGuid().ToString().Replace("/", string.Empty).Replace("+", string.Empty).Substring(0, 25);
            redID.Text = randoms;
            ///
            try
            {
                string query = ("select e_id from e_vent where (e_name = '" + EName.Text +"')");
                MySqlConnection connection = new MySqlConnection(connectionString);
                connection.Open();
                MySqlCommand command = new MySqlCommand(query, connection);
                using(MySqlDataReader reader = command.ExecuteReader())
                {

                    while (reader.Read())
                    {

                        Ecode.Text = (reader.GetString(0));

                    }

                }

            }
            catch (Exception)
            {

                MessageBox.Show("Unable to load data from database");

            }

        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
           
            try
            {
                
                MySqlConnection connection = new MySqlConnection(connectionString);
                MySqlCommand command = new MySqlCommand("INSERT INTO attendee (attend_id, a_fname, a_lname, a_email, as_name, as_district, reg_id, e_id)VALUES(@attend_id, @a_fname, @a_lname, @a_email, @as_name, @as_district, @reg_id, @e_id) on duplicate key update attend_id = attend_id, a_fname = a_fname, a_lname = a_lname, as_name = as_name, as_district = as_district ", connection);
                command.Parameters.AddWithValue("@attend_id", aID.Text);
                command.Parameters.AddWithValue("@a_fname", fName.Text);
                command.Parameters.AddWithValue("@a_lname", lName.Text);
                command.Parameters.AddWithValue("@a_email", Email.Text);
                command.Parameters.AddWithValue("@as_name", sName.Text);
                command.Parameters.AddWithValue("@as_district", SDistrict.Text);
                command.Parameters.AddWithValue("@reg_id", redID.Text);
                command.Parameters.AddWithValue("@e_id", Ecode.Text);
                connection.Open();
                command.ExecuteNonQuery();
                MessageBox.Show("Congratulations! You have created a new Attendee!");
                connection.Close();

                Attendee g = new Attendee(EName.Text);
                g.Show();
                Close();
            }
            catch (Exception)
            {

                MessageBox.Show("Database could not connect.");

            }

        }
    
        private void Email_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void sName_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void sDistrict_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void fName_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void lName_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void EName_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void Ecode_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void aID_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void redID_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        public void Insert_Click(object sender, RoutedEventArgs e)
        {

            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.FileName = "Document"; // Default file name
            dlg.DefaultExt = ".csv"; // Default file extension
            dlg.Filter = "CSV documents (.csv)|*.csv"; // Filter files by extension

            // Show open file dialog box
            bool? result = dlg.ShowDialog();

            // Process open file dialog box results
            if (result == true)
            {
                // Open document
                string filename = dlg.FileName;
                try
                {

                    StreamReader sr = new StreamReader(filename);
                    string headerLine = sr.ReadLine(); //Reads first line of file and gets it out of the way
                    bool flag = true;
                    TextFieldParser fieldParser = new TextFieldParser(sr);
                    fieldParser.TextFieldType = FieldType.Delimited;
                    fieldParser.SetDelimiters(",");
                    string[] currentRow;

                    while (!fieldParser.EndOfData)
                    {

                                currentRow = fieldParser.ReadFields();
                                MySqlConnection connection = new MySqlConnection(connectionString);
                                MySqlCommand command = new MySqlCommand("INSERT INTO attendee (attend_id, a_fname, a_lname, a_email, as_name, as_district, reg_id, e_id) VALUES(@attend_id, @a_fname, @a_lname, @a_email, @as_name, @as_district, @reg_id, @e_id) ON DUPLICATE KEY UPDATE as_name = @as_name, as_district = @as_district, a_email = @a_email, a_fname = @a_fname, a_lname = @a_lname ", connection);
                                command.Parameters.AddWithValue("@attend_id", currentRow[1]);
                                command.Parameters.AddWithValue("@a_fname", currentRow[21]);
                                command.Parameters.AddWithValue("@a_lname", currentRow[22]);
                                command.Parameters.AddWithValue("@a_email", currentRow[23]);
                                command.Parameters.AddWithValue("@as_name", currentRow[26]);
                                command.Parameters.AddWithValue("@as_district", currentRow[27]);
                                command.Parameters.AddWithValue("@reg_id", currentRow[2]);
                                command.Parameters.AddWithValue("@e_id", Ecode.Text);
                                connection.Open();
                                command.ExecuteNonQuery();
                                connection.Close();
                                if (flag)
                                {

                                  MessageBox.Show("File Is being uploaded! Please wait at least 1 minute to close the program.");
                                    //DISPLAY ONLY ONE TIME.
                                }
                               flag = false;

                    }

                }
                catch(Exception ex)
                {

                    MessageBox.Show(ex.Message);

                }
               
            }
             
        }

        private void print_Click(object sender, RoutedEventArgs e)
        {

            string value = EName.Text;
            AttendeeList w = new AttendeeList(value);
            w.Show();
            Close();

        }
    }
}
